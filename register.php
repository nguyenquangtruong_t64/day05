<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Đăng ký</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css"
        rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" href="register.css">
</head>

<body>

    <fieldset class="signup-background">
        <?php
    
    if (!empty($_POST['btnSubmit'])) {
        session_start();
        $error = 1;
        $_SESSION['address'] = $_POST['address'];
        if (!empty($_FILES['image'])) {
            move_uploaded_file($_FILES["image"]["tmp_name"], './' . $_FILES["image"]["name"]);
      
            $_SESSION['image'] = "./" . $_FILES["image"]["name"];
          }
                
        if (empty(($_POST["fullName"]))){
            echo "<div style='color: red;'>Hãy nhập tên.</div>";
            $error = 0;
        }
        else {					
            $_SESSION['fullName'] = $_POST['fullName'];
        }

        if (empty($_POST["gender"])) {
            echo "<div style='color: red;'>Hãy chọn giới tính.</div>";
            $error = 0;
        }
        else {					
            $_SESSION['gender'] = $_POST['gender'];
        }

        if (empty(($_POST["department"]))){
            echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
            $error = 0;
        }
        else {					
            $_SESSION['department'] = $_POST['department'];
        }
        
        if (empty(($_POST["birthday"]))){
            echo "<div style='color: red;'>Hãy nhập ngày sinh.</div>";
            $error = 0;
        }
        if (!validateDate($_POST["birthday"]) && !empty($_POST["birthday"])) {
            echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng.</div>";
            $error = 0;
        }
        else {					
            $_SESSION['birthday'] = $_POST['birthday'];
        }
        if($error == 1){
            header("Location: ./validate.php");
        }
    }

    function checkInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        return $data;
    }

    function validateDate($date, $format = 'd/m/Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    ?>
        <div class="signup">

            <form action="register.php" method="POST" id="form" enctype="multipart/form-data">
                <div class="signup-form">
                    <p class="signup-form-text">
                        Họ và tên
                        <span style="color: red">*</span>
                    </p>

                    <input name="fullName" type="text" class="input-text">
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Giới tính
                        <span style="color: red">*</span>
                    </p>

                    <div class="gender">
                    <?php
                    $gender = array("0"=>"Nam","1"=>"Nữ");
                    for ($i = 0; $i <= 1; $i++) {
                        if($i%2 == 0) {
                        echo '<input value="'.$gender[$i].'" id="'.$gender[$i].'" style="margin-left:10px" type="radio" name="gender">'.$gender[$i];
                        echo '<input value="'.$gender[$i+1].'" id="'.$gender[$i+1].'" style="margin-left:30px" type="radio" name="gender">'.$gender[$i+1];
                        }
                    }
                    ?>          
                    </div>

                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Phân khoa
                        <span style="color: red">*</span>
                    </p>

                    <select name='department'>
                    <?php
                        $subject = array(""=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
                        foreach($subject as $key => $val){
                            $selected = ($key == ' ') ? 'selected="selected"' : '';
                            echo '<option  value="'. $val .'" ' . $selected . ' >'. $val .'</option>';
                        }
                    ?>
                    </select>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Ngày sinh
                        <span style="color: red">*</span>
                    </p>
                    <input type="text" name="birthday" id="birthday" class="input-birthday" placeholder="dd/mm/yyyy">
                    <script type="text/javascript">
                    $(".birthday").datepicker({
                        format: "dd/mm/yyyy",
                    });
                    </script>

                </div>

                <div class="signup-form">
                    <p class="signup-form-text">
                        Địa chỉ
                    </p>
                    <input type="text" name="address" id="address" class="input-text">
                </div>


                <div class="signup-form">
                    <p class="signup-form-text">
                        Hình ảnh
                    </p>
                    <input type="file" name="image" id="image">
                </div>

                <div class="signup-form signup-submit">
                    <input type="submit" value="Đăng Kí" name="btnSubmit">
                </div>
            </form>
        </div>

    </fieldset>
</body>
</html>