<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Xác nhận</title>
    <link rel="stylesheet" href="validate.css">
</head>

<body>
    <fieldset class="signup-background">
        <div class="signup">
            <?php
                session_start();
                $fullName = isset($_SESSION['fullName']) ? $_SESSION['fullName'] : "";
                $gender = isset($_SESSION['gender']) ? $_SESSION['gender'] : "";
                $department = isset($_SESSION['department']) ? $_SESSION['department'] : "";
                $birthday = isset($_SESSION['birthday']) ? $_SESSION['birthday'] : "";
                $address = isset($_SESSION['address']) ? $_SESSION['address'] : "";
                $image = isset($_SESSION['image']) ? $_SESSION['image'] : "";
            ?>
 
            <form method="POST" id="form" enctype="multipart/form-data">
                <div class="signup-form">
                    <p class="signup-form-text">
                        Họ và tên
                    </p>

                    <div class="input-text"> <?php echo $fullName?> </div>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Giới tính
                    </p>

                    <div class="input-text"> <?php echo $gender?> </div>

                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Phân khoa
                    </p>
                    <div class="input-text"> <?php echo $department?> </div>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Ngày sinh
                    </p>

                    <div class="input-text"> <?php echo $birthday?>


                    </div>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Địa chỉ
                    </p>
                    <div class="input-text"> <?php echo $address?>
                    </div>
                </div>


                <div class="signup-form">
                    <p class="signup-form-text">
                        Hình ảnh
                    </p>
                    <div class="input-text-img">
                        <div>
                        <?php
                            echo '<span><img src="' . $image . '" height="55px" width="55px"></span>'
                        ?>
                        </div>
                    </div>
                </div>

                <div class="signup-form signup-submit">
                    <input type="submit" value="Xác nhận">
                </div>
            </form>
        </div>

    </fieldset>


</body>

</html>